"""
Script for quadratic interpolation.
Modified by: Robert Von Zimmerman

variables xi and yi are numpy arrays.
Each array holds data for coordinates in the form (xi, yi).
The solve function returns the coefficients for the polynomial in an array of 3 items.
The function is then plotted using matplotlib
"""

import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

# Set up linear system to interpolate through data points:

# Data points:
xi = np.array([-1., 1., 2])
yi = np.array([0., 4., 3.])

#A is defined using items in the array xi
A = np.array([[1., xi[0], xi[0]**2],
              [1., xi[1], xi[1]**2],
              [1., xi[2], xi[2]**2]])
b = yi

# Solve the system:
c = solve(A,b)

print "The polynomial coefficients are:"
print c

# Plot the resulting polynomial:
x = np.linspace(-2,3,1001)   # points to evaluate polynomial
y = c[0] + c[1]*x + c[2]*x**2

plt.figure(1)       # open plot figure window
plt.clf()           # clear figure
plt.plot(x,y,'b-')  # connect points with a blue line

# Add data points  (polynomial should go through these points!)
plt.plot(xi,yi,'ro')   # plot as red circles
plt.ylim(-2,8)         # set limits in y for plot

plt.title("Data points and interpolating polynomial")

plt.savefig('hw2a.png')   # save figure as .png file
