
"""
Module for quadratic and cubic interpolation.
Modified by: Robert Von Zimmerman

Collection of functions to plot and interpolate a set of coordinates.
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:

    ### Fill in this part to compute c ###
    A = np.vstack([np.ones(3), xi, xi**2]).T
    b = yi

    # Solve the system:
    c = solve(A,b)
    return c

def cubic_interp(xi, yi):
    """
    Cubic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2,3.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 c[3]*x**3.

    """
    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 4"
    assert len(xi)==4 and len(yi)==4, error_message

    # Set up linear system to interpolate through data points:

    ### Fill in this part to compute c ###
    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T
    b = yi

    # Solve the system:
    c = solve(A,b)
    return c

def plot_cubic(xi, yi):
    '''
    Plots the cubic interpolation of a given set of coordinates.

    passes the input xi, yi to cubic_interp, returning the coefficients of the polynomial:
        p(x) = xi[0] + xi[1]*x + xi[2]*x**2 + xi[3]*x**3
    The limits of the plot are found by using the minimum and maximum values found in xi[i] and yi[i].
    '''
    c = cubic_interp(xi, yi)
    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3

    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line

    # Add data points  (polynomial should go through these points!)
    plt.plot(xi,yi,'ro')   # plot as red circles
    plt.ylim(yi.min()-1,yi.max()+1)         # set limits in y for plot

    plt.title("Data points and interpolating polynomial")

    plt.savefig('cubic.png')   # save figure as .png file

def plot_quad(xi, yi):
    """
    Plots the quadratic interpolation of a given set of coordinates.

    passes the input xi, yi to quad_interp, returning the coefficients of the polynomial:
        p(x) = xi[0] + xi[1]*x + xi[2]*x**2
    The limits of the plot are found by using the minimum and maximum values found in xi[i] and yi[i].
    """
    c = quad_interp(xi, yi)
    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2

    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line

    # Add data points  (polynomial should go through these points!)
    plt.plot(xi,yi,'ro')   # plot as red circles
    plt.ylim(yi.min()-1,yi.max()+1)         # set limits in y for plot

    plt.title("Data points and interpolating polynomial")

    plt.savefig('quadratic.png')   # save figure as .png file


def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_quad2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-20., 5., 2.])
    yi = np.array([-15., 7., 34.])

    c = quad_interp(xi, yi)
    c_true = np.array([ 47.50909091,  -5.85636364,  -0.44909091])
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, %s, Expected: c= %s" % (c, c_true)
    print "c =      ", c
    print "c_true = ", c_true
def test_cubic1():
    """
    Test code, no return value or exception if test runs properly.
    """

    xi = np.array([-5., 2., 1., 0.])
    yi = np.array([-2., 5., -2., 1.])
    c = cubic_interp(xi, yi)
    c_true = np.array([ 1. , -6.4,  2.6,  0.8])

    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, %s, Expected: c= %s" % (c, c_true)
    print "c =      ", c
    print "c_true = ", c_true

if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()

