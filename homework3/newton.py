# $UWHPSC/codes/homework3/test_code.py
# To include in newton.py

def solve(fvals, x0, debug=False):
    iters = 0
    x = x0
    if debug:
        print "Initial guess: x = %22.15e" % x
    for i in range(0, 20):
        fx, fpx = fvals(x)
        deltax = fx/fpx
        if abs(fx) < 1e-14:
            iters = i
            break
        x = x - deltax
        if debug:
            print "After %i iterations, x = %22.15e" % (i, x)
    return x, iters

def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def test1(debug_solve=True):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x,iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x
