! $UWHPSC/codes/fortran/newton/test1.f90

program intersections

    use newton, only: solve
    use functions, only: f_intersect, fprime_intersect

    implicit none
    real(kind=8) :: x, x0, fx
    real(kind=8) :: x0vals(4)
    integer :: iters, itest
	logical :: debug         ! set to .true. or .false.

    debug = .false.

    ! values to test as x0:
    x0vals = (/-2.2d+0, -1.6d+0, -0.8d+0, 1.4d+0 /)

    do itest=1,4
        x0 = x0vals(itest)
		print *, ' '  ! blank line
        call solve(f_intersect, fprime_intersect, x0, x, iters, debug)

        fx = f_intersect(x)
        print 12, x0
12      format('With initial guess:', e22.15)

        print 11, x, iters
11      format('solver returns x = ', e22.15, ' after', i3, ' iterations')

        enddo

end program intersections
