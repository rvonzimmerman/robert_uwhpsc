! $UWHPSC/codes/fortran/newton/functions.f90

module functions

COMMON Pi
REAL :: pi = 2*ACOS(0.0)
contains
real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)

    implicit none
    real(kind=8), intent(in) :: x

    fprime_sqrt = 2.d0 * x

end function fprime_sqrt

real(kind=8) function f_intersect(x)
    implicit none
    real(kind=8), intent(in) :: x
    f_intersect = 0.60d+0 * x**2.0d+0 + x*cos(pi * x) - 1.0d+0

end function f_intersect

real(kind=8) function fprime_intersect(x)
    implicit none
    real(kind=8), intent(in) :: x
    fprime_intersect = -pi * x * sin(pi * x) + 1.2d+0 * x + cos(pi * x)

end function fprime_intersect

end module functions
