from newton import solve
from numpy import cos, sin, pi, linspace
from pylab import plot, savefig

test = False #this is used to check answers against example

def fvals_intersection(x):
    f = .6*x**2 +x*cos(pi*x) - 1
    fp = -pi*x*sin(pi*x) + 1.2*x + cos(pi*x)
    return f, fp

def fvals_test_intersection(x):
    '''
    Example function for testing intersection code
    '''
    f = x**2 + sin(x) - 1
    fp = 2*x + cos(x)
    return f, fp
x_points = []
for x0 in [-2.2, -1.6, -.8, 1.4]:
    fx, iters  = solve(fvals_intersection, x0)
    print "With initial guess: %22.15e \nsolve returns: %22.15e after %i iterations." % (x0, fx, iters)
    x_points.append(fx)
if test:
    print "These are the example points!"
    for x0 in [-.5, .5]:
        fx, iters  = solve(fvals_test_intersection, x0)
        print "With initial guess: %22.15e \n solve returns: %22.15e after %i iterations." % (x0, fx, iters)

x = linspace(-5, 5, 1000)
g1 = x*cos(pi*x)
g2 = 1 - .6*x**2

for i in x_points:
    plot(i, i*cos(pi*i), 'ko')

plot(x,g1)
plot(x,g2)

savefig('intersections.png')
