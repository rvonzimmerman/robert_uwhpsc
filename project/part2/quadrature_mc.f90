module quadrature_mc

    implicit none
    contains

        function quad_mc(g, a, b, ndim, points)

            implicit none
            real (kind = 8) :: quad_mc, rdm, f
            integer, intent(in) :: ndim, points
            real (kind=8), dimension(ndim), intent(in) :: a, b
            real (kind=8), dimension(points,ndim) :: x
            real(kind=8), external :: g
            integer :: i, j, cur_value
            do i=1, points
                do j = 1, ndim
                    call random_number(rdm)
                    x(i,j) = 2 + rdm*2 ! make  numbers fit in interval
                enddo
            enddo
            quad_mc = 0.d0
            f = 0.d0
            do i = 1, points
                f = f + g(x(i,:), ndim)
            enddo
            quad_mc = 2**ndim * f / points
        end function quad_mc

end module quadrature_mc
