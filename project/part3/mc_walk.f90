module mc_walk
    use problem_description
    implicit none
    integer :: seed1

contains

subroutine random_walk(i0, j0, max_steps, ub, iabort)
    implicit none
    integer, intent(in) :: i0, j0, max_steps
    integer :: i, j, k, istep
    real (kind=8), dimension(max_steps) :: r
    real (kind=8), intent(out) :: ub
    integer, intent(out) :: iabort
    real (kind=8) :: xb, yb

    i = i0
    j = j0
    do k = 1, max_steps
       call random_number(r(k))
    enddo

    do istep = 1, max_steps
        if (r(istep).lt.0.25d0) then
            i = i - 1
        else if (r(istep).lt.0.5d0) then
            i = i + 1
        else if (r(istep).lt.0.75d0) then
            j = j - 1
        else
            j = j + 1
        endif
        ! Check if boundary is hit
        if ((i*j*(nx+1-i)*(ny+1-j)).eq.0.d0) then
            xb = ax + i*dx
            yb = ay + j*dy
            ub = uboundary(xb, yb)
            iabort = 0
            exit
        else if (istep.eq.(max_steps-1)) then
            print *, "Did not hit boundary breh"
            iabort = 1
            exit
        endif
    enddo
    return
end subroutine random_walk
subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
    implicit none
    integer, intent(in) :: i0, j0, max_steps, n_mc
    real (kind=8) :: ub, ub_sum
    real (kind=8), intent(out) :: u_mc
    integer, intent(out) :: n_success
    integer :: k, i, j, iabort
    ub_sum = 0.d0
    n_success = 0
    do k=1, n_mc
        i = i0
        j = j0
        call random_walk(i0, j0, max_steps, ub, iabort)
        if (iabort.eq.0) then
            ub_sum = ub_sum + ub
            n_success = n_success + 1
        endif
    enddo
    u_mc = ub_sum / n_success
    return
end subroutine many_walks
end module mc_walk
