
program laplace_mc
use problem_description
use random_util, only: init_random_seed
use mc_walk
implicit none
real (kind=8) :: u_true, x0, y0, u_mc, error, u_mc_total, u_sum_old
real (kind=8) ::  u_sum_new, ub_sum
integer :: n_mc, n_walks, n_success, max_steps, i0, j0, k,n_total

    open(unit=25, file='mc_walk_error.txt', status='unknown')
    seed1 = 12345
    call init_random_seed(seed1)
    x0 = 0.9d0
    y0 = 0.6d0

    !should probably figure out to round this
    i0 = ceiling((x0-ax)/dx)
    j0 = ceiling((y0-ay)/dy)
    n_mc = 10
    max_steps = 100*max(nx, ny)
    x0 = ax + i0*dx
    y0 = ay + j0*dy
    u_true = utrue(x0,y0)

    print *, u_true

    call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)

    error = abs((u_mc - u_true) / u_true)

    print 10, n_success, u_mc, error
    10 format(i8, d24.15, 2d13.3)

    !start accumulating totals
    u_mc_total = u_mc
    n_total = n_success
    do k=1, 12
        u_sum_old = u_mc_total * n_total
        call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
        u_sum_new = u_mc * n_success
        n_total = n_total + n_success
        u_mc_total = (u_sum_old + u_sum_new) / n_total
        error = ABS((u_mc_total - u_true) / u_true)

        print 11, n_total, u_mc_total, error
        11 format(i8, d24.15, 2d13.3)
        write(25,'(i10,e23.15,e15.6)') n_total, u_mc_total, error
        n_mc = 2*n_mc
    enddo
    print *, "final approximation to u(x0, y0) ", u_mc_total
    print *, "total number of walks ", n_total
 end program laplace_mc
