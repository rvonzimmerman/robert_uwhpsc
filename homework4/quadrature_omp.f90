! ~/amath/robert_uwhpsc/homework4/quadrature_omp.f90

module quadrature
use omp_lib

contains

real(kind=8) function trapezoid(f, a, b, n)
    implicit none
    integer :: i
    integer, intent(in) :: n
    real(kind=8) :: x, h, s
    real(kind=8), external :: f
    real(kind=8), intent(in) :: a, b


    h = (b-a)/(n)
    !$omp parallel do private(x)
    do i=1,n-1
        x = a +h*i
        s = s + f(x)
    enddo
    trapezoid = h*(f(a)/2.d0 + s + f(b)/2.d0)

end function trapezoid

subroutine error_table(f, a, b, nvals, int_true)
    integer, dimension(:), intent(in) :: nvals
    real(kind=8), external :: f
    real(kind=8) :: a, b, int_true, int_trap, last_error
    print *, "    n         trapezoid            error       ratio"
    last_error = 0.d0
    do i=1,size(nvals)
        int_trap = trapezoid(f, a, b, nvals(i))
        error = (int_trap - int_true)
        ratio = last_error / error
        last_error = error
        print 11, nvals(i), int_trap, error, ratio
    11  format(i8, es22.14, es13.3, es13.3)
    enddo
end subroutine error_table

end module quadrature
